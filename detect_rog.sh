#!/bin/bash
#

cd /var/lib/nagios
logname="/var/lib/nagios/rogue_dhcp_$(date +"%Y-%m-%d_%H%M%S").log"

sudo /usr/local/sbin/detect_rogue_dhcp > $logname 2>/dev/null

remaining=$(cat $logname | grep remaining|awk '{print $8}' )

if [ "$remaining" -ne 0 ]; then
  output="$(cat /tmp/last)"
else
  output=$(cat $logname | grep -P ^\([0-9a-fA-F][0-9a-fA-F]:\){5}\([0-9a-fA-F][0-9a-fA-F]\) | grep -v 10.102.15.65 )
  echo "$output">/tmp/last
fi


[ -e "$logname" ] && rm "$logname"

count=$(echo "$output"| grep -v ^$ |uniq| wc -l)

if [ "$count" -gt 0 ]; then
 ips=$(echo "$output" | awk '{print $2}' |uniq| tr '\n' ';'|sed '$s/.$//')

 if [ "$count" -eq 1 ]; then
   echo "$ips-rogue DHCP server found"
 else
   echo "[$ips]: $count rogue dhcp servers found"
 fi

 # return warning

 exit 1
else
 echo "No rogue DHCP servers"

 # return OK
 exit 0
fi
